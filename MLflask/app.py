from flask import Flask, request, jsonify, render_template, session, redirect, url_for, session
import requests
import pandas as pd
import numpy as np
import joblib


app = Flask(__name__, template_folder='template')
@app.route('/',  methods = ['GET','POST'])
def home():
    if request.method == 'POST':
     
        # Get values through input bars
        pclass = request.form["pclass"]
        name = request.form["name"]
        sex = request.form["sex"]
        age = request.form["age"]
        sibsp = request.form["sibsp"]
        parch = request.form["parch"]
        ticket = request.form["ticket"]
        fare = request.form["fare"]
        cabin = request.form["cabin"]
        embarked = request.form["embarked"]

        return redirect(url_for('result',pclass=pclass, name=name, sex=sex, age=age, sibsp=sibsp, parch=parch, ticket=ticket, fare=fare, cabin=cabin, embarked=embarked))
    return render_template('website.html')


@app.route('/result/<int:pclass>/<string:name>/<string:sex>/<int:age>/<int:sibsp>/<int:parch>/<string:ticket>/<int:fare>/<string:cabin>/<string:embarked>', methods = ['GET','POST'])
def result(pclass, name, sex, age, sibsp, parch, ticket, fare, cabin, embarked):
    # Put inputs to dataframe
    clf = joblib.load("cfk.pkl") 
    X = pd.DataFrame([[pclass, name, sex, age, sibsp, parch, ticket, fare, cabin, embarked]], columns = ['pclass', 'name', 'sex', 'age', 'sibsp', 'parch', 'ticket', 'fare', 'cabin', 'embarked'])
    # Get prediction
    prediction = clf.predict(X)
    if prediction == 1:
        prediction = "Survived"
    else:
        prediction = "No Survived"
    return render_template('result.html', res = prediction)





if __name__ =='__main__':
    app.run(debug=True,host="0.0.0.0", port=int("5000"))


int