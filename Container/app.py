from flask import Flask, request, jsonify, render_template, session, redirect, url_for, session
import requests
import pandas as pd
import numpy as np
import joblib


app = Flask(__name__, template_folder='template')
@app.route('/',  methods = ['GET','POST'])
def home():
    if request.method == 'POST':
        # Unpickle classifier
        clf = joblib.load("cfk.pkl") 
        # Get values through input bars
        pclass = request.form.get("pclass")
        name = request.form.get("name")
        sex = request.form.get("sex")
        age = request.form.get("age")
        sibsp = request.form.get("sibsp")
        parch = request.form.get("parch")
        ticket = request.form.get("ticket")
        fare = request.form.get("fare")
        cabin = request.form.get("cabin")
        embarked = request.form.get("embarked")
        # Put inputs to dataframe
        X = pd.DataFrame([[pclass, name, sex, age, sibsp, parch, ticket, fare, cabin, embarked]], columns = ['pclass', 'name', 'sex', 'age', 'sibsp', 'parch', 'ticket', 'fare', 'cabin', 'embarked'])
        # Get prediction
        prediction = clf.predict(X)

    else:
        prediction = ""
    if prediction == 1:
        prediction = "survived"
    else:
        prediction = "nosurvived"
    return render_template("website.html", output = prediction)





if __name__ =='__main__':
    app.run(debug=True,host="0.0.0.0", port=int("5000"))